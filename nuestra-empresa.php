<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Nuestra Empresa </title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- Bootstrap core css-->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="css/owl.theme.default.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/aboutus_style_1.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>


    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1>Nuestra Empresa</h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Nuestra Empresa </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->

    <!-- section_1 start-->
    <div class="section_1">
        <div class="container">
            <div class="row">
                <div class="about_slider_wrapper">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="about_image">
                                <img src="images/about_us/aboutus1.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="about_image">
                                <img src="images/about_us/aboutus2.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="about_image">
                                <img src="images/about_us/aboutus3.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 col-lg-offset-2">
                    <div class="about_text_wrapper">
                        <p>“Somos una empresa de tipo familiar fundada en 1980.

Desde nuestros orígenes nos dedicamos a la importación y distribución mayorista en todo el Uruguay, de productos e insumos para el sector agro-veterinario.

Hemos introducido al país varias marcas que hoy gozan de un muy buen prestigio en el mercado uruguayo. Entre ellas destacamos Allflex, Plyrap, Mecano Ganadero, Gripple, Thunderbird, Wile, Solartec, Feed Plast, Silotex, Supervet.

A la fecha, nuestros productos se comercializan en más de 400 puntos de venta de insumos agro veterinarios, diseminados en ciudades y pueblos de todo el Uruguay.

Es nuestro objetivo constante, el ofrecer a nuestros clientes insumos de calidad, confiables, y brindarles un excelente servicio post venta en todas las líneas que representamos.”</p>
                        <h5> - <span> Veronica </span> (Directora) </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section_1 end-->

    <!-- counter section start -->
    <div class="counter_section">
        <div class="counter_section_overlay"></div>
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 col-lg-offset-3">
                    <div class="section_heading">
                        <h2>Somos Expertos</h2>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="our-progress">
                                <div class="chart_1st" data-percent="95">
                                    <span class="percent percent-one">95</span>
                                </div>
                                <h4><a href="#">Ganaderia</a></h4>

                            </div>
                            <!-- /.our-progress -->
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="our-progress">
                                <div class="chart_2nd" data-percent="80">
                                    <span class="percent percent-two">80</span>
                                </div>
                                <h4><a href="#">Nutricion Animal</a></h4>

                            </div>
                            <!-- /.our-progress -->
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="our-progress">
                                <div class="chart_3rd" data-percent="90">
                                    <span class="percent percent-three">90</span>
                                </div>
                                <h4><a href="#">Energia Solar</a></h4>

                            </div>
                            <!-- /.our-progress -->
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="our-progress">
                                <div class="chart_4th" data-percent="60">
                                    <span class="percent percent-four">60</span>
                                </div>
                                <h4><a href="#">Cercos</a></h4>

                            </div>
                            <!-- /.our-progress -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.counter section end -->

    <!-- client_section start -->
    <div class="client_section">
        <div class="container">
            <div class="client_slider">
				<div class="owl-carousel owl-theme">
					<div class="item">
						<img src="images/about_us/logo-client-1.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-2.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-3.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-4.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-5.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-6.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-7.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-8.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-9.png" alt="">
					</div>
					
				</div>
            </div>
        </div>
    </div>
    <!-- client_section end -->

    <?php require('footer.php') ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Owl Carousel js -->
    <script src="js/owl.carousel.js"></script>
    <!-- Magnific Popup js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Counter js -->
    <script src="js/jquery.easypiechart.min.js"></script>
    <!-- Progress js -->
    <script src="js/jquery.inview.min.js"></script>
    <!-- aboutus js -->
    <script src="js/aboutus.js"></script>
    <!-- Custom js -->
    <script src="js/custom.js"></script>

</body>

</html>