<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Accesorios </title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
	<!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/shop.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

   <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>
	
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1> Accesorios </h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Accesorios </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->
	
	<!-- shop_fulwidth_wrapper start -->
    <div class="shop_fulwidth_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="shop_full_width">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tab-content btc_shop_index_content_tabs_main">
                                    <div id="grid" class="tab-pane fade in active">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">
                                                        
                                                        <img src="images/shop/arreadores.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class=></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                    <li><a href="#"><i class=></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">ARREADORES DE GANADO <br>Chica 60 cm. | Mediana 84 cm. | Grande 100cm.</a></h1>
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/carretel-plastico.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">CARRETEL <br>PLÁSTICO</a></h1>
                                                       
                                                      <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/carretel-reforzado.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">CARRETEL PLÁSTICO REFORZADO<br>Capacidad 750m. de hilo. Presentación: vacíos | 500 m. hilo 7 filamentos.</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/toma-de-tierra.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">kit toma <br>de tierra</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/voltimetro.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Voltímetro</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/manija-max.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Manija p/Portera MAX</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/portera.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Portera elástica</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/manija-plastica.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Manija plastica</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/llave-de-corte.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Llave de corte</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/Kit-Derivador-de-Rayos.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Kit derivador de rayos</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="btc_shop_indx_cont_box_wrapper">
                                                    <div class="btc_shop_indx_img_wrapper">

                                                        <img src="images/shop/probador-de-bateria.jpg" alt="shop_img" class="img-responsive" />
                                                        <div class="cc_li_img_overlay">
                                                            <div class="cc_li_img_text">
                                                                <ul>
                                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>

                                                                    <li><a href="#"><i class="fa fa-expand"></i></a></li>
                                                                </ul>
                                                          </div>
                                                      </div>
                                                  </div>

                                                    <div class="btc_shop_indx_img_cont_wrapper">

                                                        <h1><a href="#">Probador de batería</a></h1>
                                                        
                                                        <div class="cc_li_cont_wrapper">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- shop sidebar end -->
	
    <?php require('footer.php') ?>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	<!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <!-- Custom js -->
    <script src="js/shop.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>