    <!-- footer start -->

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-12 col-sm-6">
                    <div class="widget_1">
                        <div class="widget1_image_wrapper">
                            <img src="images/dist-si-2.png" class="img-responsive" alt="footer_logo_img" />
                        </div>
                        <div class="widget1_text_wrapper">
                            <div class="text_wrapper_first">
                                <p>Distribuidora SI ltda. Es una empresa de tipo familiar fundada en 1980.</p>
                            </div>
                            <div class="text_wrapper_second">
                                <p>Desde nuestros orígenes nos dedicamos a la importación y distribución mayorista en todo el Uruguay, de productos e insumos para el sector agro-veterinario.</p>
                            </div>
                            <div class="widget1_link">
                                <a href="nuestra-empresa.php"> Ver Mas <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <ul class="widget1_social_icons">
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                            </li>
                            <li> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                            </li>
                            <li> <a href="#"><i class="fa fa-vimeo-square" aria-hidden="true"></i> </a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12 col-sm-6">
                    <div class="widget_2">
                        <h4>Links de Interes</h4>
                        <ul class="widget2_list">
                            <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Ipsum. Proin gravida nibh vel</a>
                            </li>
                            <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Auctor aliquet. Aenean </a>
                            </li>
                            <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Din, lorem quis bibendum </a>
                            </li>
                            <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Nisi elit consequat ipsum,</a>
                            </li>
                            <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Sagittis sem nibh id elit. </a> </li>
                        </ul>
                    </div>
                </div>                
                <div class="col-lg-3 col-md-3 col-xs-12 col-sm-6">
                    <div class="widget_2">
                        <h4>Contacto</h4>
                        <ul class="widget2_list">
                            <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Galicia 1129, Montevideo - Uruguay</a>
                            </li>
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 2900 9093* </a>
                            </li>
                            <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> siltda@adinet.com.uy </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- footer end -->

    <div class="copyright_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <div class="copyright_content">
                        <p>© Copyright 2019 para <a href="index.php"> Distribuidora Sí </a>- todos los derechos reservador</p>
                    </div>
                </div>
        </div>
    </div>
    <!-- copyright_wrapper end -->