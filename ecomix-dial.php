<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> ECOMIX PLASTICA DIAL 50cc.</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/shop.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

 
    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1> ECOMIX PLASTICA DIAL 50cc. </h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> ECOMIX PLASTICA DIAL 50cc. </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->
	
    <!-- CC ps top product Wrapper Start -->
    <div class="cc_ps_top_product_main_wrapper">
        <div class="container">
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="video_img_section_wrapper">
						<div class="video_nav_img hidden-xs">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/ecomix-dial.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/ecomix-dial-1.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/ecomix-dial-2.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            
						</div>
                        <div class="cc_ps_top_slider_section">
                            <div class="owl-carousel owl-theme">
                                <div class="item" data-hash="zero">

                                    <img class="small img-responsive" src="images/shop/ecomix-dial.jpg" alt="small_img" />

                                </div>
                                <div class="item" data-hash="one">

                                    <img class="small img-responsive" src="images/shop/ecomix-dial-1.jpg" alt="small_img" />

                                </div>
                                <div class="item" data-hash="two">

                                    <img class="small img-responsive" src="images/shop/ecomix-dial-2.jpg" alt="small_img" />

                                </div>
                                
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="video_nav_img visible-xs">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/li_img1.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/li_img2.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/li_img3.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/li_img4.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="btc_shop_single_prod_right_section">
                        <h1>ECOMIX PLASTICA DIAL 50cc. </h1>
                        <div class="btc_shop_sin_pro_icon_wrapper">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                           
                            <h5>Las jeringas ECOMIX se fabrican bajo los más altos estándares de calidad. Los materiales utilizados, bronce y zamak, llevan un tratamiento de niquelado para extender su durabilidad. Diseños anatómicos. Cuerpo plástico irrompible graduado y esterilizable por ebullición</h5>
                            <h5>Sistema Ecomix Dial.</h5>
                            <h5>Dosis exacta</h5>
                            <ul>
                            <h5>REPUESTOS INTERCAMBIABLES</h5>
                                <li><i class="fa fa-long-arrow-right"></i> 1 - Mango grande</li>
                                <li><i class="fa fa-long-arrow-right"></i> 2 - Mango chico</li>
                                <li><i class="fa fa-long-arrow-right"></i> 3 - Resorte de mango</li>
                                <li><i class="fa fa-long-arrow-right"></i> 4 - Tornillo y tuerca de mango</li>
                                <li><i class="fa fa-long-arrow-right"></i> 5 - Kit gatillo completao</li>
                                <li><i class="fa fa-long-arrow-right"></i> 6 - Pico champion</li>
                                <li><i class="fa fa-long-arrow-right"></i> 7 - Tubo de plástico</li>
                                <li><i class="fa fa-long-arrow-right"></i> 8 - Escala graduadora</li>
                                <li><i class="fa fa-long-arrow-right"></i> 9 - Boton de ajuste</li>
                                <li><i class="fa fa-long-arrow-right"></i> 10 - Kit graduador Dial</li>
                                <li><i class="fa fa-long-arrow-right"></i> 11 - Kit juego de goma x50cc.</li>
                                <li><i class="fa fa-long-arrow-right"></i> 12 - Tubo y tornillo metalico</li>
                                                                
                            </ul>
                        </div>
                        
                        
                </div>
            </div>
        </div>
    </div>
    <!-- CC ps top product Wrapper End -->
	
	<!-- accordion section start -->
	
    <!-- accordion section end -->
    
  
	
    <?php require('footer.php') ?>
	
	<!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/megnify.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
	<script src="js/cloud-zoom.1.0.3.js"></script>
    <!-- Custom js -->
    <script src="js/shop.js"></script>
    <script src="js/custom.js"></script>
    <script>
	
		function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }

        function goToByScroll(id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        //------- Progress Bar ---------//

        $('.progress_section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $.each($('div.progress-bar'), function() {
                    $(this).css('width', $(this).attr('aria-valuenow') + '%');
                });
                $(this).off('inview');
            }
        });
    </script>
</body>

</html>