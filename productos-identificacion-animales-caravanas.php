<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Caravanas</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <!-- Product Css -->
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/slick-theme.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/shop.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/shop_organic_product.css" rel="stylesheet">
</head>

<body>

 
    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1> Caravanas </h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Caravanas </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->
	
    <!-- CC ps top product Wrapper Start -->
    <div class="cc_ps_top_product_main_wrapper">
        <div class="container">
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="video_img_section_wrapper">
                        <div class="slider-for">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/afflex.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/caaravana-boton-n2.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/caravana-flexitag.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/caravana-n4.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#four"><img src="images/shop/caravana-n6.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#five"><img src="images/shop/caravana-n6-macho-n5.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#sixe"><img src="images/shop/caravanan-n7.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#seven"><img src="images/shop/caravanan-n7maxi.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                        </div>
                        <div class="slider-nav hidden-xs">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/afflex.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/caaravana-boton-n2.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/caravana-flexitag.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/caravana-n4.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#four"><img src="images/shop/caravana-n6.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#five"><img src="images/shop/caravana-n6-macho-n5.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#sixe"><img src="images/shop/caravanan-n7.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#seven"><img src="images/shop/caravanan-n7maxi.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="btc_shop_single_prod_right_section">
                        <h1>Caravanas Allflex</h1>
                        <div class="btc_shop_sin_pro_icon_wrapper">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                           
                            <h5>La identificación es la base para recopilar y administrar la información que permite tomar decisiones objetivas de gestión y salud, para mejorar la productividad.</h5>
                            <h5>Las Caravanas Allflex, con tratamiento contra rayos UV que evitan que se resequen y quiebren con el paso del tiempo. Diseñadas con contornos redondeados, minimizan los riesgos de pérdidas y la cabeza cerrada de la parte hembra evita posibles heridas causadas con la punta del macho.</h5>
                            <h5>Mayor flexibilidad y resistencia en poliuretano de alta calidad. Cierre más firme con el sistema rotaclip (anillo de plástico más rígido).</h5>
                            <h5>Mejor cicatrización: El sistema de cierre proporciona una total rotación permitiendo una mejor aireación y evitando apretarse contra la oreja, distancia macho/hembra garantizada.</h5>
                            <h5>Construidas con material reciclable y colorantes orgánicos, contribuimos con el respeto al medio ambiente.</h5>
                            <ul>
                            <h5>MODELOS</h5>
                                <li><i class="fa fa-long-arrow-right"></i> Caravana Botón </li>
                                <li><i class="fa fa-long-arrow-right"></i> Caravana Flexitag</li>
                                <li><i class="fa fa-long-arrow-right"></i> Caravanas Nº 4, Nº 6, Nº 7, Nº 7 maxi, Nº12 </li>
                                <li><i class="fa fa-long-arrow-right"></i> Macho Nº 5 </li>
                                <li><i class="fa fa-long-arrow-right"></i> Macho Pino </li>
                                                                
                            </ul>
                        </div>
                        
                        
                </div>
            </div>
        </div>
    </div>
    <!-- CC ps top product Wrapper End -->
	
	<!-- accordion section start -->
	
    <!-- accordion section end -->
    
  
	
    <?php require('footer.php') ?>
	
	<!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Product js -->
    <script src="js/slick.js"></script>
    <script src="js/slick.min.js"></script>

    <script src="js/megnify.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
	<script src="js/cloud-zoom.1.0.3.js"></script>
    <!-- Custom js -->
    <script src="js/shop.js"></script>
    <script src="js/custom.js"></script>
    <script>
	
		function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }

        function goToByScroll(id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        //------- Progress Bar ---------//

        $('.progress_section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $.each($('div.progress-bar'), function() {
                    $(this).css('width', $(this).attr('aria-valuenow') + '%');
                });
                $(this).off('inview');
            }
        });
    </script>
</body>

</html>