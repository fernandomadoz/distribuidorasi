    <!-- header start -->
    <div class="header">
		<div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                        <div class="contact_info_wrapper">
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope"></i> siltda@adinet.com.uy</a></li>
                                <li class="contact_number_wrapper hidden-xs"><a href="#"><i class="fa fa-phone"></i> 2900 9093*</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                        <!-- signin_and_social_icon_wrapper -->
                        <div class="signin_and_social_icon_wrapper">
                            <ul>
                                <li class="social_icon_wrapper hidden-xs">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    </ul>
                                </li>
                                <!-- Cart Option -->
                                
                                <!-- /.Cart Option -->
                            </ul>
                        </div>
                        <!-- /.signin_and_social_icon_wrapper end -->
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div>
        <div class="main_menu_wrapper hidden-xs hidden-sm">
            <nav class="navbar mega-menu navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="container">
                    <div class="navbar-header hidden-xs hidden-sm">
                        <a class="navbar-brand" href="index.php"><img src="images/dist-si-3.png" alt=""></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="nuestra-empresa.php">Nuestra Empresa</a>                                
                            </li>
                            <li class="dropdown">
                                <a href="productos.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos</a>
                                <ul class="dropdown-menu" style="min-width: 450px">
                                    <li>
                                        <a href="productos-mecano.php">
													 Instalaciones portátiles para ganado (Mecano Ganadero)
									    <span><i class="fa fa-caret-right"></i>
													  </span>
													</a>
                                      
                                  </li>
                                    <li>
                                        <a href="productos-cerco-electrico.php">
													 Cercos eléctricos y accesorios (Plyrap)
									    <span><i class="fa fa-caret-right"></i>
													  </span>
													</a>
                                        
                                  </li>
                                    <li>
                                        <a href="productos-identificacion-animales.php">
													 Identificación de animales / lectores (Alflex)
									    <span><i class="fa fa-caret-right"></i>
													  </span>
													</a>
                                        
                                  </li>
                                    <li>
                                        <a href="productos-balanzas.php">
													 Balanzas Electrónicas
									    <span><i class="fa fa-caret-right"></i>
													  </span>
													</a>
                                        
                                  </li>
                                    <li>
                                        <a href="productos-conectores-y-tensadores.php">
													 Conectores y tensadores de alambres (Gripple)
									    <span><i class="fa fa-caret-right"></i>
													  </span>
													</a>
                                        
                                  </li>
									<li>
                                        <a href="productos-instrumental-veterinario.php">
													 Jeringas Ecomix
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                                    <li>
                                        <a href="productos-comederos-bebederos-pisos-plasticos.php">
													 Comederos, bebederos y pisos plásticos (Feedplast)
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                                    <li>
                                        <a href="productos-accesorios-aguadas.php">
													 Accesorios para aguadas (Plyrap)
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                                    <li>
                                        <a href="productos-medidores-humedad.php">
													 Medidores de humedad granos/forraje/madera (Wile)
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                                    <li>
                                        <a href="productos-paneles-solares.php">
													 Paneles Solares (Solartec)
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                                    <li>
                                        <a href="productos-silos-portatiles.php">
													 Silos portátiles (Silotex)
													  <span><i class="fa fa-caret-right"></i>
													  </span>
									  </a>
                                        
                                    </li>
                              </ul>
                            </li>
                            </li>
                            <li>
                                <a href="nuestras-marcas.php">Nuestras Marcas</a>
                            </li>
                            <li>
                                <a href="contacto.php">Contacto</a>
                        </ul>
                    <!-- /.navbar-collapse -->
                </div>
            </nav>
        </div>
        <!-- .site-nav -->
        <div class="mobail_menu_main visible-xs visible-sm">
            <div class="navbar-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                            <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                            <button type="button" class="navbar-toggle collapsed" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sidebar">
                <a class="sidebar_logo" href="index.php"><img src="images/logo.png" alt=""></a>
                <div id="toggle_close">&times;</div>
                <div id='cssmenu'>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="nuestra-empresa.php">Nuestra Empresa</a></li>
                        <li class='has-sub'><a href='#'>Productos</a>
                            <ul>
                                <li><a href='productos-mecano.php'>Instalaciones portátiles para ganado (Mecano Ganadero)</a></li>
                                <li><a href='productos-cerco-electrico.php'>Cercos eléctricos y accesorios (Plyrap)</a></li>
                                <li><a href='productos-identificacion-animales.php'>Identificación de animales / lectores (Alflex)</a></li>
                                <li><a href='productos-balanzas.php'>Balanzas Electrónicas</a></li>
                                <li><a href='productos-conectores-y-tensadores.php'>Conectores y tensadores de alambres (Gripple)</a></li>
                                <li><a href='productos-instrumental-veterinario.php'>Instrumental Veterinario y accesorios</a></li>
                                <li><a href='productos-comederos-bebederos-pisos-plasticos.php'>Comederos, bebederos y pisos plásticos (Feedplast)</a></li>
                                <li><a href='productos-accesorios-aguadas.php'>Accesorios para aguadas (Plyrap)</a></li>
                                <li><a href='productos-medidores-humedad.php'>Medidores de humedad granos/forraje/madera (Wile)</a></li>
                                <li><a href='productos-paneles-solares.php'>Paneles Solares (Solartec)</a></li>
                                <li><a href='productos-silos-portatiles.php'>Silos portátiles (Silotex)</a></li>
                            </ul>
                        </li>
                        <li><a href="nuestras-marcas.php">Nuestras Marcas</a></li>
                        <li><a href="contacto.php">Contacto</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- header end -->