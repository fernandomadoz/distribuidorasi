<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Silos Portátiles </title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/shop.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

 
    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1> Silos Portátiles </h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Silos Portátiles SILOTEX </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->
	
    <!-- CC ps top product Wrapper Start -->
    <div class="cc_ps_top_product_main_wrapper">
        <div class="container">
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="video_img_section_wrapper">
						<div class="video_nav_img hidden-xs">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/li_img11.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            
                            
						</div>
                        <div class="cc_ps_top_slider_section">
                            <div class="owl-carousel owl-theme">
                                <div class="item" data-hash="zero">

                                    <img class="small img-responsive" src="images/shop/li_img11.jpg" alt="small_img" />

                                </div>
                                
                                
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="video_nav_img visible-xs">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/li_img1.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/li_img2.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/li_img3.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/li_img4.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="btc_shop_single_prod_right_section">
                        <h1>Silos Portátiles SILOTEX </h1>
                        <div class="btc_shop_sin_pro_icon_wrapper">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                           
                            <h5> Se arma y desarma fácilmente Se carga con cualquier tornillo o tolva y se descarga por gravedad a través de una válvula. Además, su flexibilidad estructural y liviandad permite maniobrar desde cualquier ángulo y extraer el cereal en su totalidad. Resistente a intemperie y a agresiones como: granizo, aves, roedores.etc. Tienen una vida útil estimada de 5 años. Conservan el grano en forma natural. Brindan el ambiente optimo a los mismos. Permite la pre-clasificación de la cosecha. Mejoran la calidad del grano guardado.
</h5>
                            <ul>
                            <h5>DATOS TECNICOS MODELO XL1</h5>
                                <li><i class="fa fa-long-arrow-right"></i> Diámetro: 4,43m</li>
                                <li><i class="fa fa-long-arrow-right"></i> Altura de carga: 2,70m</li>
                                <li><i class="fa fa-long-arrow-right"></i> Altura total: 3,64m</li>
                                <li><i class="fa fa-long-arrow-right"></i> Capacidad: 46,4 m3 </li>
                                <li><i class="fa fa-long-arrow-right"></i> Capacidad trigo ton): 39tn</li>
                                <li><i class="fa fa-long-arrow-right"></i> Capacidad maíz ton): 35tn</li>
                                <li><i class="fa fa-long-arrow-right"></i> Capacidad soja ton): 36tn</li
                                                                
                            ></ul>
                        </div>
                        
                        
                </div>
            </div>
        </div>
    </div>
    <!-- CC ps top product Wrapper End -->
	
	<!-- accordion section start -->
	
    <!-- accordion section end -->
    
  
	
    <?php require('footer.php') ?>
	
	<!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/megnify.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
	<script src="js/cloud-zoom.1.0.3.js"></script>
    <!-- Custom js -->
    <script src="js/shop.js"></script>
    <script src="js/custom.js"></script>
    <script>
	
		function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }

        function goToByScroll(id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        //------- Progress Bar ---------//

        $('.progress_section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $.each($('div.progress-bar'), function() {
                    $(this).css('width', $(this).attr('aria-valuenow') + '%');
                });
                $(this).off('inview');
            }
        });
    </script>
</body>

</html>