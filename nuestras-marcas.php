<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Nuestras Marcas </title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- Bootstrap core css-->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="css/owl.theme.default.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/aboutus_style_1.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>


    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1>Nuestras Marcas</h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Nuestras Marcas </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->

    <!-- section_1 start-->
    <div class="section_1">
              <div class="about_text_wrapper">
                    <h5> - <span> Nuestras Marcas nos respaldan </span> - </h5>
                        </div>
                        <p>“Todas las marcas que comercializamos gozan de un muy buen prestigio en el mercado uruguayo. Entre ellas destacamos”</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section_1 end-->


    <div class="clientTwo ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-1.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-2.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-3.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-4.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-5.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-6.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-7.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-8.png" alt="">
                        </a>
                    </div>
              </div>
                    <div class="col-sm-6 col-md-3 pd-2">
                        <a href="#"><img src="images/about_us/logo-client-9.png" alt="">
                        </a>
                    </div>
      </div>
            </div>
        </div>
    <!--end of clientTwo -->

    <?php require('footer.php') ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Owl Carousel js -->
    <script src="js/owl.carousel.js"></script>
    <!-- Magnific Popup js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Counter js -->
    <script src="js/jquery.easypiechart.min.js"></script>
    <!-- Progress js -->
    <script src="js/jquery.inview.min.js"></script>
    <!-- aboutus js -->
    <script src="js/aboutus.js"></script>
    <!-- Custom js -->
    <script src="js/custom.js"></script>

</body>

</html>