<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Caja de Palpación </title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.theme.default.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="css/shop.css" rel="stylesheet">
    <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

 
    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!-- page_header start -->
    <div class="page_header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-6">
                    <h1> Caja de Palpación </h1>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                    <div class="sub_title_section">
                        <ul class="sub_title">
                            <li> <a href="#"> Home </a> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
                            <li> Caja de Palpación </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page_header end -->
	
    <!-- CC ps top product Wrapper Start -->
    <div class="cc_ps_top_product_main_wrapper">
        <div class="container">
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="video_img_section_wrapper">
						<div class="video_nav_img hidden-xs">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/magnum.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/electrificadores-magnum.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/py5.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc_ps_tabs">
                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/tabla-MAGNUM.jpg" class="img-responsive" alt="nav_img"></a>
                            </div>
						</div>
                        <div class="cc_ps_top_slider_section">
                            <div class="owl-carousel owl-theme">
                                <div class="item" data-hash="zero">

                                    <img class="small img-responsive" src="images/shop/magnum.jpg" alt="small_img" />

                                </div>
                                <div class="item" data-hash="one">

                                    <img class="small img-responsive" src="images/shop/electrificadores-magnum.jpg" alt="small_img" />

                                </div>
                                <div class="item" data-hash="two">

                                    <img class="small img-responsive" src="images/shop/py5.jpg" alt="small_img" />
                                </div>
                                <div class="item" data-hash="three">
                                    <img class="small img-responsive" src="images/shop/tabla-MAGNUM.jpg" alt="small_img" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="video_nav_img visible-xs">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#zero"><img src="images/shop/li_img1.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#one"><img src="images/shop/li_img2.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#two"><img src="images/shop/li_img3.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 cc_ps_tabs">
                                                <a class="button secondary url owl_nav" href="#three"><img src="images/shop/li_img4.jpg" class="img-responsive" alt="nav_img"></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="btc_shop_single_prod_right_section">
                        <h1>Caja de Palpación</h1>
                        <div class="btc_shop_sin_pro_icon_wrapper">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                           
                            <h5>Se ajusta al ancho del corredor.</h5>
                            <h5>Usada tambien como puerta de salida.</h5>
                            
                        </div>
                        
                        
                </div>
            </div>
        </div>
    </div>
    <!-- CC ps top product Wrapper End -->
	
	<!-- accordion section start -->
	<div class="accordion_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 accordion_one">
                    <div class="panel-group" id="accordionFourLeft">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion_oneLeft" href="#collapseFiveLeftone" aria-expanded="false">
                                Descripcíon de producto
                              </a>
                            </h4>
                            </div>
                            <div id="collapseFiveLeftone" class="panel-collapse collapse in" aria-expanded="false" role="tablist">
                                <div class="panel-body">
                                    <div class="img-accordion">
                                        <img src="images/shop/shop_acc.jpg" alt="">
                                    </div>
                                    <div class="text-accordion">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit risus nisl, nec facilisis ante iaculis fringilla. Integer mattis risusadipiscing elit. Phasellus hendrerit risus nisl, nec facilisis ante iaculis fri ngilla. Integer mattis risus vel dapibus rhoncus. Duis ut nulla et metus vehicula facilisis non eu quam. vel dapibus rhoncus. Duis ut nulla et metus vehicula facilisis non eu quam.
                                        </p>
                                        <ul class="accordian_list_item">
                                            <li><i class="fa fa-long-arrow-right"></i> Lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
                                            <li><i class="fa fa-long-arrow-right"></i> Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.</li>
                                            <li><i class="fa fa-long-arrow-right"></i> Lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end of panel-body -->
                            </div>
                        </div>
                        <!-- /.panel-default -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion_oneLeft" href="#collapseFiveLeftThree" aria-expanded="false">
                               Detalles del producto
                              </a>
                            </h4>
                            </div>
                            <div id="collapseFiveLeftThree" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                <div class="panel-body">

                                    <div class="text-accordion">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit risus nisl, nec facilisis ante iaculis fringilla. Integer mattis risus vel dapibus rhoncus. Duis ut nulla et metus vehicula facilisis non eu quam facilisis ante iaculis fringilla. Integer mattis risus vel dapibus rhoncus. Duis ut nulla.
                                        </p>
                                        <ul class="accordian_list_item">
                                            <li><i class="fa fa-long-arrow-right"></i> Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin.</li>
                                            <li><i class="fa fa-long-arrow-right"></i> Lorem quis bibendum auctor, nisi elit consequat ipsum.</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end of panel-body -->
                            </div>
                        </div>

                        <!-- /.panel-default -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion_oneLeft" href="#collapseFiveLeftfour" aria-expanded="false">
                               Etiquetado sobre el producto 
                              </a>
                            </h4>
                            </div>
                            <div id="collapseFiveLeftfour" class="panel-collapse collapse" aria-expanded="false" role="tablist">
                                <div class="panel-body">

                                    <div class="text-accordion">
                                        <div class="tag_cloud_wrapper">
                                            <ul>
                                                <li>
                                                    <a href="#">Business</a>
                                                </li>
                                                <li>
                                                    <a href="#">Corporate</a>
                                                </li>
                                                <li>
                                                    <a href="#">Services</a>
                                                </li>
                                                <li>
                                                    <a href="#">Customer</a>
                                                </li>
                                                <li>
                                                    <a href="#">Money</a>
                                                </li>
                                                <li class="active">
                                                    <a href="#">Portfolio</a>
                                                </li>
                                                <li>
                                                    <a href="#">Psd</a>
                                                </li>
                                                <li>
                                                    <a href="#">Joomla</a>
                                                </li>
                                                <li>
                                                    <a href="#">Skills</a>
                                                </li>
                                                <li>
                                                    <a href="#">Partners</a>
                                                </li>
                                                <li>
                                                    <a href="#">Wordpress</a>
                                                </li>
                                                <li>
                                                    <a href="#">Html</a>
                                                </li>
                                                <li>
                                                    <a href="#">Psd</a>
                                                </li>
                                                <li>
                                                    <a href="#">Joomla</a>
                                                </li>
                                                <li>
                                                    <a href="#">Skills</a>
                                                </li>

                                                <li>
                                                    <a href="#">Joomla</a>
                                                </li>
                                                <li>
                                                    <a href="#">Skills</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of panel-body -->
                            </div>
                        </div>
                        <!-- /.panel-default -->
                        

                        <!--end of /.panel-group-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- accordion section end -->
    
  
	
    <?php require('footer.php') ?>
	
	<!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>
    <script src="js/megnify.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
	<script src="js/cloud-zoom.1.0.3.js"></script>
    <!-- Custom js -->
    <script src="js/shop.js"></script>
    <script src="js/custom.js"></script>
    <script>
	
		function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }

        function goToByScroll(id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }

        //------- Progress Bar ---------//

        $('.progress_section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $.each($('div.progress-bar'), function() {
                    $(this).css('width', $(this).attr('aria-valuenow') + '%');
                });
                $(this).off('inview');
            }
        });
    </script>
</body>

</html>