<!DOCTYPE html>
<!-- 
Template Name: A-Future HTML
Version: 1.0.0
Author: Webstrot
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Distribuidora Sí</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Animation Css -->
    <link href="css/animate.css" rel="stylesheet">
     <!-- Style Css -->
     <link href="sliders_basic/assets/css/basic_slider_8.css" rel="stylesheet">


     <!-- Web Fonts -->
     <link href="sliders_basic/assets/css/fonts.css" rel="stylesheet">
     <!-- Animation Css -->
     <link href="sliders_basic/assets/css/animate.css" rel="stylesheet">
     <!-- Camera Css -->
     <link href="sliders_basic/assets/css/camera.css" rel="stylesheet">
     <!-- Style Css -->
     <link href="sliders_basic/assets/css/basic_slider_8.css" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="css/owl.theme.default.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Magnific Popup Css -->
    <link href="css/magnific-popup.css" rel="stylesheet">
    <!-- Style Css -->
    <link href="css/homepage_style_2.css" rel="stylesheet">
        <!-- Common Style CSS -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

    <a href="javascript:" id="return-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <?php require('top.php') ?>

    <!--Slider area start here-->
        <section class="float_left hidden-xs">
                <div id="theme-main-banner">
                    
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-1.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Todos los días trabajamos<br>Para la Ganadería de nuestro país</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s"> Creemos en lo que hacemos, confiamos en lo que ofrecemos. Trabajamos intensamente para el desarrollo la producción ganadera de nuestro suelo.</p>
                                <a href="nuestra-empresa.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-2.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Todos los días trabajamos<br>Para la Ganadería de nuestro país</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s"> Creemos en lo que hacemos, confiamos en lo que ofrecemos. Trabajamos intensamente para el desarrollo la producción ganadera de nuestro suelo.</p>
                                <a href="nuestra-empresa.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-3.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Corrales <br>Versatiles y Robustos</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s">Ofrecemos la línea mas completa de soluciones para optimizar el manejo de ganado.</p>
                                <a href="productos-mecano.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-4.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Compactos Solares <br>La Herramienta mas eficaz</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s">Para el control del ganado, es la herramienta exeocional, que facilita optimizar recursos y tiempo.</p>
                                <a href="productos-cerco-electrico.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                </div> <!-- /#theme-main-banner -->
            </section>
    <!--Slider area end here-->

    <!--Slider area start here-->
        <section class="float_left hidden-lg hidden-sm hidden-md">
                <div id="theme-main-banner2">
                    
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-1-movil.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Todos los días trabajamos<br>Para la Ganadería de nuestro país</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s"> Creemos en lo que hacemos, confiamos en lo que ofrecemos. Trabajamos intensamente para el desarrollo la producción ganadera de nuestro suelo.</p>
                                <a href="nuestra-empresa.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-2-movil.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Todos los días trabajamos<br>Para la Ganadería de nuestro país</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s"> Creemos en lo que hacemos, confiamos en lo que ofrecemos. Trabajamos intensamente para el desarrollo la producción ganadera de nuestro suelo.</p>
                                <a href="nuestra-empresa.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-3-movil.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Corrales <br>Versatiles y Robustos</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s">Ofrecemos la línea mas completa de soluciones para optimizar el manejo de ganado.</p>
                                <a href="productos-mecano.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                    <div data-src="sliders_basic/assets/images/basic_slider_8/banner-slider-4-movil.jpg">
                        <div class="camera_caption">
                            <div class="container">
                                <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Compactos Solares <br>La Herramienta mas eficaz</h1>
                                <p class="wow fadeInUp animated" data-wow-delay="0.4s">Para el control del ganado, es la herramienta exeocional, que facilita optimizar recursos y tiempo.</p>
                                <a href="productos-cerco-electrico.php" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ver Mas!</a>
                            </div> <!-- /.container -->
                        </div> <!-- /.camera_caption -->
                    </div>
                </div> <!-- /#theme-main-banner -->
            </section>
    <!--Slider area end here-->

                
    <!-- client_section start -->
       <!-- client_section start -->
    <div class="client_section">
        <div class="container">
            <div class="client_slider">
				<div class="owl-carousel owl-theme" style="z-index: 0">
					<div class="item">
						<img src="images/about_us/logo-client-1.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-2.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-3.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-4.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-5.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-6.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-7.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-8.png" alt="">
					</div>
					<div class="item">
						<img src="images/about_us/logo-client-9.png" alt="">
					</div>
					
				</div>
            </div>
        </div>
    </div>
    <!-- client_section end -->

    <?php require('footer.php') ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Bootstrap js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Portfolio Filter js -->
    <script src="js/jquery.shuffle.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <!-- Counter Pie Chart js -->
    <script src="js/jquery.easypiechart.min.js"></script>
    <!-- Magnific Popup js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Owl Carousel js -->
    <script src="js/owl.carousel.js"></script>
    <!-- wow js -->
    <script src="js/wow.js"></script>
	<!-- portfolio filter js -->
    <script src="js/portfolio.js"></script>
	<!-- homepage js -->
    <script src="js/homepage.js"></script>


    <script src="sliders_basic/assets/js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="sliders_basic/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="sliders_basic/assets/js/owl.carousel.js" type="text/javascript"></script>
    <script src="sliders_basic/assets/js/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="sliders_basic/assets/js/camera.min.js" type="text/javascript"></script>
    <script src="sliders_basic/assets/js/custom.js" type="text/javascript"></script>

</body>

</html>